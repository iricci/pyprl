import warnings
import inspect
import os
import sys

try:
    from pyprl.logger import setup_logging, clear_logging
    from pyprl.strategy import Parallel, Serial
    from pyprl.cache import Cache, TaskCache
except:
    sys.path.append('../pyprl/')
    from logger import setup_logging, clear_logging
    from strategy import Parallel, Serial
    from cache import Cache, TaskCache

    
_strategy_db = {'serial': Serial, 'parallel': Parallel}


class Executor:
    def __init__(self, tasks=None, parameters=None, caching=False, verbose=False):
        self.tasks = tasks
        """Task list to execute"""
        self.parameters = parameters
        """Parameter list to pass to tasks"""
        self.results = []
        """Simulation results"""
        
        # handle None as task list
        if self.tasks == None:
            self.tasks = []
        # Convert single task to list
        elif not isinstance(self.tasks, list):
            self.tasks = [tasks]
            
        # Caching
        self.cache_out = '.pyprl'
        """Caching directory"""
        self.caching = caching
        """Caching toggle; if specified, integrated caching is used"""
        if self.caching:
            self.cache = Cache(self.cache_out)
            
        # Logging
        self.logger_out = 'simulation.log'
        """Logger file output"""
        self.logger = setup_logging(name='executor', level=10, filename=self.logger_out, stdout=verbose)
        """Current logger"""
            
    def taskify(self, task):
        """Decorator to add function to task list"""
        self.tasks.append(task)

    def run(self, strategy='parallel', **kwargs):
        """Run tasks using selected strategy, with given parameters"""

        # perform checks on tasks and parameters
        
        # generate task list if a single task is provided with multiple parameters
        # NOTE: parameters have priority. If a single task is provided
        #       and j parameters are available, the task will be cloned
        #       j-1 times
        if len(self.tasks) == 1 and len(self.parameters) > 1:
            task = self.tasks[0]
            self.tasks = [task for _ in range(len(self.parameters))]

        # sanity check on task callability
        for task in self.tasks:
             if not callable(task):
                 raise RuntimeError(f'{task.__name__} is not a callable')

        # sanity check on list size/shape
        assert len(self.tasks) == len(self.parameters), f'ntask != nparameters: {self.ntasks} != {self.nparameters}'

        # enable caching if required
        if self.caching:
            self.tasks = [TaskCache(task, cache=self.cache) for task in self.tasks]
        # load strategy
        if strategy in _strategy_db.keys():
            self.strategy = _strategy_db[strategy]
        else:
            assert inspect.isclass(strategy), f'method must be in{list(_strategy_db.keys())} or class'
            self.strategy = strategy

        # run
        self.strategy = self.strategy(**kwargs)
        self.results = self.strategy.run(self.tasks, self.parameters)

    def parallel(self, mode='process'):
        """Legacy wrapper for run method using parallel strategy"""
        warnings.warn("use run(strategy='parallel', mode=...)", DeprecationWarning)
        self.run(strategy='parallel', mode=mode)

    def serial(self):
        """Legacy wrapper for run method using serial strategy"""
        warnings.warn("use run(strategy='serial')", DeprecationWarning)
        self.run(strategy='serial')
