"""
Caching works like this 

SCHEDULER -> init -> initialize Cache object
CACHE -> init: create directory for caching
SCHEDULER -> run -> call STRATEGY and pass Cache object
STRATEGY -> Parallel -> run -> if cache is available, check if future is returned and write to cache

On rerun:
STRATEGY -> Parallel -> run -> check if future name is in cache; if so, check if task changed and load results
"""
import os
import shutil
import inspect
import pickle
import time
from hashlib import md5


def get_kwargs_func(func, parameters):
    """Build kwargs dictionary"""
    # Get func signature and build args, kwargs data
    signature = inspect.signature(func)
    arg_names = tuple(param.name
                      for param in signature.parameters.values()
                      if param.default is param.empty)
    kwargs_names = tuple(param.name
                      for param in signature.parameters.values()
                      if param.default is not param.empty)
    default_kwargs = {param.name: param.default
                      for param in signature.parameters.values()
                      if param.default is not param.empty}

    # Check if parameters is iterable, and cast to one
    try:
        _ = iter(parameters)
    except TypeError:
        parameters = tuple([parameters])
        
    # Generate arguments dictionary if parameters is not a dict
    if not isinstance(parameters, dict):
        actual_args = {}
        actual_args.update({name: arg for name, arg in zip(arg_names, parameters)})
    else:
        actual_args = parameters

    # Check actual arguments against full signature
    if tuple(actual_args.keys()) != arg_names+kwargs_names:
        # Update with missing kwargs
        actual_args.update(default_kwargs)

    return actual_args        


def get_parameters_from_args(func, args, kwargs):
    """Convert arguments and keyword arguments to parameters dictionary"""
    signature = inspect.signature(func)
    arg_names = tuple(param.name
                      for param in signature.parameters.values()
                      if param.default is param.empty)

    parameters = {name: arg  for name, arg in zip(arg_names, args)}
    parameters.update(kwargs)
    return parameters


class Cache:
    """
    Generate cache and store function results. 
    Inspired by pantarei's caching approach.
    """

    def __init__(self, path):
        self.path = path
        """Cache path"""
        
        # Create cache directory
        os.makedirs(self.path, exist_ok=True)

    def _cache_path(self, name):
        return os.path.join(self.path, name)
        
    def generate_name(self, func, parameters):
        """
        Generate unique function name from arguments
        """
        # Get actual kwargs
        kwargs = get_kwargs_func(func, parameters)
        # Sort kwargs
        kwargs_sorted = {key: kwargs[key] for key in sorted(kwargs)}
        kwargs_bytes = str(kwargs_sorted).encode()

        return md5(kwargs_bytes).hexdigest()

    def create(self, name, func, parameters):
        """Create cache for name"""
        # Generate path
        path = self._cache_path(name)
        # Create directory
        os.makedirs(path, exist_ok=True)
        # Get proper kwargs
        kwargs = get_kwargs_func(func, parameters)
        # Store arguments
        with open(os.path.join(path, 'args.pkl'), 'wb') as f:
            pickle.dump(kwargs, f)

    def write(self, name, results):
        """Write function output to cache"""
        # Get path
        path = self._cache_path(name)
        # Pickle and save results
        with open(os.path.join(path, 'results.pkl'), 'wb') as f:
            pickle.dump(results, f)
    
    def read(self, name):
        """Read function output from cache"""
        # Get path
        path = self._cache_path(name)
        with open(os.path.join(path, 'results.pkl'), 'rb') as f:
            results = pickle.load(f)
        return results
    
    def search(self, name):
        """Look for cache in cache path"""
        path = self._cache_path(name)
        return os.path.exists(os.path.join(path, 'results.pkl'))

    def list(self):
        """List cache contents"""
        return os.listdir(self.path)
            
    def remove(self, name):
        """Remove cache for a specific task."""
        shutil.rmtree(self._cache_path(name))

    def clear(self, risky=False):
        """Empty cache directory"""
        if not risky:
            print('REMOVING cache in 5 seconds...')
            time.sleep(5)
        else:
            print('REMOVING cache NOW')
        shutil.rmtree(self.path)
                

class TaskCache:
    """Caching for a single task"""

    def __init__(self, func, cache=None):
        self.func = func
        """Function to be cached"""
        self.cache = cache
        """Cache to use. If no cache is provided, run without caching"""

    def __call__(self, *args, **kwargs):
        """Run task using cache"""

        # Run normally if no cache is available
        if self.cache is None:
            return self.func(*args, **kwargs)
        
        # From args and kwargs to actual parameters dict
        parameters = get_parameters_from_args(self.func, args, kwargs)
        parameters = get_kwargs_func(self.func, parameters)

        # Generate name
        name = self.cache.generate_name(self.func, parameters)
        # Create cache for task
        self.cache.create(name, self.func, parameters)
        # Look for task in cache and run if search fails
        if self.cache.search(name):
            # Load results from cache
            results = self.cache.read(name)
        else:
            # Run task
            results = self.func(*args, **kwargs)
        # Store results in cache
        self.cache.write(name, results)
        return results
