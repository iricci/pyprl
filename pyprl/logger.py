import sys
import logging


class _MyFormatter(logging.Formatter):
    def format(self, record):
        if record.levelname in ['WARNING', 'ERROR']:
            return '# ' + record.levelname + ' ' + record.msg % record.args
        return '# ' + record.msg % record.args


def setup_logging(name=None, level=40, stdout=False, filename=None, update=False):
    """Logging API, fork of `atooms.core.utils`."""
    if name is None:
        log = logging.getLogger()
    else:
        log = logging.getLogger(name)

    if update:
        # We only update the level of the logger
        log.setLevel(level)
    else:
        # The logger should always pass messages to all handlers
        current_level = log.getEffectiveLevel()
        log.setLevel(min(level, current_level))

    formatter = _MyFormatter()

    # Handlers
    console_handler = logging.StreamHandler(sys.stdout)
    file_handler = logging.FileHandler(filename)

    if filename is None:
        log.addHandler(console_handler)
    else:
        log.addHandler(file_handler)

    if stdout is True:
        log.addHandler(console_handler)

    for l in log.handlers:
        l.setFormatter(formatter)
        l.setLevel(level)

    return log

def clear_logging():
    import logging
    log = logging.getLogger()
    for h in log.handlers[-2:]:
        log.removeHandler(h)
