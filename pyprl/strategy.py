import logging
from datetime import datetime
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

_parallel_db = {'process': ProcessPoolExecutor, 'thread': ThreadPoolExecutor}

# get executor logger
logger = logging.getLogger('executor')


class StrategyBase:
    """Base class for run strategies"""

    def __str__(self):
        """Strategy name"""
        pass
    
    def __init__(self):
        self.results = []
        """List of run results"""
                 
    def run(self, tasks, parameters):
        """Perform run using specified task and parameter list"""
        pass

    
class Parallel(StrategyBase):

    def __init__(self, mode='process', max_workers=3):
        self.mode_db = _parallel_db
        """Database containing run modes"""
        self.max_workers = max_workers
        """Maximum number of allowed parallel workers"""

        # instantiate parent constructor
        super().__init__()
        
        # processing mode handling
        # NOTE: if no database is provided, mode selection is skipped altogether
        if self.mode_db is not None:
            if mode in self.mode_db.keys():
                self.run_mode = self.mode_db[mode]
            else:
                assert hasattr(mode, '__call__'), f'method must be in{list(self.mode_db.keys())} or callable'
                self.run_mode = mode
        
    def __str__(self):
        return "run type: parallel"

    def _done_callback(self, future):
        """Callback for future completion"""
        logger.debug("{} : {} completed".format(datetime.now(), future.name))
        
    def run(self, tasks, parameters):
        """Run (in parallel) multiple simulation using ready-made metadata."""

        # Pool
        futures = []
        with self.run_mode(max_workers=self.max_workers) as pool:
            if self.max_workers is None:
                max_workers = pool._max_workers - 1
            logger.debug("{} : {} tasks to be submitted to pool.".format(datetime.now(), len(tasks)))
            for idx, task, parameter in zip(range(len(tasks)), tasks, parameters):
                if isinstance(parameter, dict):
                    f = pool.submit(task, **parameter)
                else:
                    f = pool.submit(task, parameter)
                f.name = "T-{}".format(idx)
                f.add_done_callback(self._done_callback)
                futures.append(f)
            logger.debug("{} : all tasks submitted to pool.".format(datetime.now()))
            self.results = [future.result() for future in futures if not future.cancelled()]
            logger.debug("{} : {} tasks completed successfully".format(datetime.now(), len(self.results)))

        return self.results


class Serial(StrategyBase):

    def __init__(self):
        super().__init__()

    def __str__(self):
        return "run type: serial"
        
    def run(self, tasks, parameters):
        """Run (in series) multiple simulation using ready-made metadata."""

        logger.debug("{} : {} tasks to be processed.".format(datetime.now(), len(tasks)))
        for idx, task, parameter in zip(range(len(tasks)), tasks, parameters):
            if isinstance(parameter, dict):
                self.results.append(task(**parameter))
            else:
                self.results.append(task(parameter))
            logger.debug("T-{} completed at {}".format(idx, datetime.now()))
        logger.debug("\n{} tasks completed successfully".format(len(tasks)))

        return self.results
