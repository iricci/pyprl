import unittest
import os
import numpy as np

try:
    from pyprl.cache import Cache
except:
    # This allows running the test within the tests folder
    import sys
    sys.path.append('../pyprl/')
    from cache import Cache
    
def mock_task_1(x):
    return x*x

class Test(unittest.TestCase):

    def setUp(self):
        self.cache_path = os.path.join(os.getcwd(), '.cache_test')
        
    def test_cache_creation(self):
        """Test cache creation for single task"""
        # Instantiate cache
        cache = Cache(self.cache_path)
        parameter = 1.
        # Get func name and create cache entry
        name = cache.generate_name(mock_task_1, parameter)
        cache.create(name, mock_task_1, parameter)
        # Compare with expected paths
        expected_path = os.path.join(self.cache_path, name)
        assert os.path.exists(os.path.join(self.cache_path, name)), 'task path in cache does not exist'
        assert os.path.exists(os.path.join(self.cache_path, name+'/args.pkl')), 'args file does not exist'
        # Clear artifacts
        cache.clear(risky=True)

    def test_cache_storage(self):
        """Test cache reading and writing"""
        # Instantiate cache
        cache = Cache(self.cache_path)
        parameter = 1.
        # Get func name and create cache entry
        name = cache.generate_name(mock_task_1, parameter)
        cache.create(name, mock_task_1, parameter)
        # Run task
        result = mock_task_1(parameter)
        # Write results to cache
        cache.write(name, result)
        # Check cache for .pkl files
        assert os.path.exists(os.path.join(self.cache_path, name+'/results.pkl')), 'no results file found'
        # Read cache and compare with result
        cache_result = cache.read(name)
        assert result == cache_result, 'cached result do not match actual results'
        # Clear artifacts
        cache.clear(risky=True)

    def test_cache_search(self):
        """Test cache searching"""
        # Instantiate cache
        cache = Cache(self.cache_path)
        parameters = np.arange(0, 3, 1)
        # Iterate over parameters
        for parameter in parameters:
            # Get func name and create cache entry
            name = cache.generate_name(mock_task_1, parameter)
            cache.create(name, mock_task_1, parameter)
            # Run task and write to cache
            result = mock_task_1(parameter)
            cache.write(name, result)
            assert cache.search(name), f'cache for mock_task_1 with {parameter=} cannot be found using available search; returned {cache.search(name)}'
            
        # Clear artifacts
        cache.clear(risky=True)

    def test_cache_remove(self):
        """Test cache searching"""
        # Instantiate cache and name list
        cache = Cache(self.cache_path)
        parameters = np.arange(0, 3, 1)
        names = []
        # Iterate over parameters
        for parameter in parameters:
            # Get func name and create cache entry
            name = cache.generate_name(mock_task_1, parameter)
            names.append(name)
            cache.create(name, mock_task_1, parameter)
        # Remove one task
        cache.remove(names[1])
        assert not os.path.exists(os.path.join(self.cache_path, names[1])), f'task {names[1]} cache was not removed'
        # Clear artifacts
        cache.clear(risky=True)
            
    def tearDown(self):
        try:
            import shutil
            shutil.rmtree(self.cache_path)
        except FileNotFoundError:
            pass

if __name__ == '__main__':
    unittest.main()
