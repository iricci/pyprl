import unittest
import os

try:
    from pyprl.executor import Executor
except:
    # This allows running the test within the tests folder
    import sys
    sys.path.append('../pyprl/')
    from executor import Executor as Executor

def mock_task_1(x):
    return x*x

def mock_task_2(x, y=1):
    return [x*x, y]

class Test(unittest.TestCase):

    def setUp(self):
        self.ntasks = 100
        self.tasks = [mock_task_1 for _ in range(self.ntasks)]
        self.tasks_kwargs = [mock_task_2 for _ in range(self.ntasks)]
        self.parameters = [i for i in range(self.ntasks)]
        self.parameters_dicts = [{'x': i, 'y': 0} for i in range(self.ntasks)]
        self.results = [i*i for i in range(self.ntasks)]
        self.results_kwargs = [[i, 0] for i in range(self.ntasks)]

    def test_serial(self):
        """Test serial execution of tasks"""
        executor = Executor(tasks=self.tasks, parameters=self.parameters)
        executor.run(strategy='serial')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results), 'computed results do not match exact results'

    def test_parallel_process(self):
        """Test parallel execution of tasks using `concurrent.futures.ProcessPoolExecutor"""
        executor = Executor(tasks=self.tasks, parameters=self.parameters)
        executor.run(strategy='parallel', mode='process')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results), 'computed results do not match exact results'

    def test_parallel_thread(self):
        """Test parallel execution of tasks using `concurrent.futures.ThreadPoolExecutor"""
        executor = Executor(tasks=self.tasks, parameters=self.parameters)
        executor.run(strategy='parallel', mode='thread')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results), 'computed results do not match exact results'

    def test_serial_with_kwargs(self):
        """Test serial execution of tasks with keyword arguments"""
        executor = Executor(tasks=self.tasks_kwargs, parameters=self.parameters_dicts)
        executor.run(strategy='serial')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results_kwargs), 'computed results do not match exact results'

    def test_parallel_process_with_kwargs(self):
        """Test parallel execution of tasks with keyword arguments using `concurrent.futures.ProcessPoolExecutor"""
        executor = Executor(tasks=self.tasks_kwargs, parameters=self.parameters_dicts)
        executor.run(strategy='parallel', mode='process')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results_kwargs), 'computed results do not match exact results'

    def test_parallel_process_with_kwargs(self):
        """Test parallel execution of tasks with keyword arguments using `concurrent.futures.ProcessPoolExecutor"""
        executor = Executor(tasks=self.tasks_kwargs, parameters=self.parameters_dicts)
        executor.run(strategy='parallel', mode='process')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results_kwargs), 'computed results do not match exact results'

    def test_parallel_thread_with_kwargs(self):
        """Test parallel execution of tasks with keyword arguments using `concurrent.futures.ThreadPoolExecutor"""
        executor = Executor(tasks=self.tasks_kwargs, parameters=self.parameters_dicts)
        executor.run(strategy='parallel', mode='thread')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results_kwargs), 'computed results do not match exact results'

    def test_parallel_process_caching(self):
        """Test parallel execution of tasks using `concurrent.futures.ProcessPoolExecutor and caching"""
        executor = Executor(tasks=self.tasks, parameters=self.parameters, caching=True)
        executor.run(strategy='parallel', mode='process')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results), 'computed results do not match exact results'
        assert len(executor.cache.list()) != 0, 'caching list is empty'

        # Clear cache
        executor.cache.clear()
        
    def test_parallel_thread_caching(self):
        """Test parallel execution of tasks using `concurrent.futures.ThreadPoolExecutor and caching"""
        executor = Executor(tasks=self.tasks, parameters=self.parameters, caching=True)
        executor.run(strategy='parallel', mode='thread')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results), 'computed results do not match exact results'
        assert len(executor.cache.list()) != 0, 'caching list is empty'

        # Clear cache
        executor.cache.clear()

    def test_serial_caching(self):
        """Test parallel execution of tasks using `concurrent.futures.ThreadPoolExecutor and caching"""
        executor = Executor(tasks=self.tasks, parameters=self.parameters, caching=True)
        executor.run(strategy='serial')

        assert len(executor.tasks) == self.ntasks, f'{len(executor.tasks)} != {self.ntasks}'
        assert len(executor.parameters) == self.ntasks, f'{len(executor.parameters)} != {self.ntasks}'
        assert len(executor.results) == self.ntasks, f'{len(executor.results)} != {self.ntasks}'
        assert all(executor.results) == all(self.results), 'computed results do not match exact results'
        assert len(executor.cache.list()) != 0, 'caching list is empty'

        # Clear cache
        executor.cache.clear()
        
    def tearDown(self):
        log_file_path = os.path.join(os.getcwd(), 'simulation.py')
        if os.path.exists(log_file_path):
            os.remove(log_file_path)


if __name__ == '__main__':
    unittest.main()
