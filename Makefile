PACKAGE = pyprl

.PHONY: install test coverage docs pep8 clean

all: install

install:
	pip install .

test:
	python -m unittest discover -s tests

coverage:
	coverage run -m unittest discover -s tests
	coverage report

pep8:
	autopep8 -r -i $(PACKAGE)
	flake8 $(PACKAGE)

docs:
	pdoc -o docs/api --force --html --skip-errors $(PACKAGE)
	make -C docs/ html

clean:
	find $(PROJECT) tests -name '__pycache__' -name '*.pyc' -exec rm '{}' +
	rm -rf build/ dist/


