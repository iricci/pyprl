# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project (roughly) adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.1](https://framagit.org/iricci/pyprl/compare/0.3.0...0.3.1) - 2024-05-28

### Added

- Add UID to task name generation (useful to distinguish tasks with same `kwargs` in cache) [`02cd091`](https://framagit.org/iricci/pyprl/-/commit/02cd091041a49a5b0e44dc5182f6b4e7ce1694a2)
- Add cache module [`02f2af5`](https://framagit.org/iricci/pyprl/-/commit/02f2af51a88f45aad04fe8e3dd5e09343aba8989)

## [0.3.0](https://framagit.org/iricci/pyprl/compare/0.3.0...0.3.0) - 2024-04-21

### Added

- Add task decorator and refactor [`d63483c`](https://framagit.org/iricci/pyprl/commit/d63484c029c8c0ff9791d868fa32b5005c9aa61f)
- Add changelog [`95bfc0b`](https://framagit.org/iricci/pyprl/commit/95bfc0b7909a3202d905834325f2385a978aae92)

## [0.2.0](https://framagit.org/iricci/pyprl/compare/0.1.0...0.2.0) - 2024-04-03

### Added

- Pass single functions as tasks when multiple parameters are specified [`91ee0c1`](https://framagit.org/iricci/pyprl/commit/91ee0c1daf7e1223d594b2020f76f60bdcab3ef2)
- Enable logging to file by default via `logger.py` module [`91ee0c1`](https://framagit.org/iricci/pyprl/commit/91ee0c1daf7e1223d594b2020f76f60bdcab3ef2)
- Pass keyword arguments to tasks via dictionaries as `parameters` [`4f87eeb`](https://framagit.org/iricci/pyprl/commit/4f87eeb23d876efab8934b026f94b22052736c2e)
- Check list size when `Executor` is instantiated [`c2aebe5`](https://framagit.org/iricci/pyprl/commit/c2aebe54266593d73bb28fa55bb47ac81bf8ec1f)

## 0.1.0 - 2024-04-03
