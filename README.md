pyprl - a (very) basic task runner
---------------------------
**pyprl** runs a list of tasks (*e.g.* Python functions) consecutively or in parallel using the `concurrent.futures` module from the standard Python library. 

# Quick start

Prepare a task list to run, *e.g.*

```
import numpy as np

def mock_task(x, verbose=False):
    """Mock task to perform"""
    initial = []
    for i in range(1000000):
        initial.append(i*x)
    if verbose:
        print('Stop mocking me!')

    return np.array(initial)
```

Instantiate the executor, and add tasks using a task list

```
from pyprl.executor import Executor

tasks = [mock_task for _ in range(100)]
parameters = list(range(100))

executor = Executor(tasks=tasks, parameters=parameters)
```

...or decorating the function itself

```
@executor.taskify
def mock_task_2(x, verbose=False):
	return mock_task(x, verbose=verbose)
```

Run the tasks in parallel

```

executor.parallel()
print(executor.results)
```

...or in series

```
executor.series()
print(executor.results)
```

...or using the `run` method

```
# Run in series
executor.run(strategy='series')

# Run in parallel
executor.run(strategy='parallel', mode='process')
```

Run the same task as many times as there are parameters

```
executor = Executor(tasks=mock_task, parameters=parameters)
executor.parallel()
```

Use dictionaries to pass keyword arguments

```
parameters = [{'x': i, 'verbose': True} for i in range(100)]
executor = Executor(tasks=mock_task, parameters=parameters)
executor.parallel()
```

# Installation

From the code repository
```
git clone https://framagit.org/iricci/pyprl.git
cd pyprl
make install
```

# To-do

 - [x] Pre-populate task list if a single callable is provided
 - [x] Perform checks on task list type
 - [x] Perform checks on task and parameter list size and shape
 - [x] Enable use of dicts for task kwargs
 - [x] Provide decoupled interface for different serial and parallel runners
 - [x] Add tests and CI
 - [x] Add package info
 - [x] Add decorator for task
 - [ ] Add documentation
 - [ ] Explore (and add) joblib support
 - [ ] Add task monitor
 - [ ] Add API

 # Author

 - [Iacopo Ricci](https://iricci.frama.io)
